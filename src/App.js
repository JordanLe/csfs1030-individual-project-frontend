import React, { useState } from 'react'
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Navigation from './components/shared/Navigation';
import Footer from './components/shared/Footer'
import Homepage from './components/pages/Homepage';
import Portfolio from './components/pages/Portfolio';
import Resume from './components/pages/Resume';
import Contact from './components/pages/Contact';
import Login from './components/pages/Login';
import AddUser from './components/pages/AddUser';
import PrivateRoute from './components/shared/PrivateRoute'
import Listings from './components/pages/Listing'
import EditPortfolio from './components/pages/EditPortfolio.js'


function App () {
  
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  //store name & email data once logged in
  //const [userData, setUserData] = useState({email:""});

  //retrieve from session storage if logged in user has super admin privleges
  //const [isAdmin, setIsAdmin] = useState(false);

  //logout function
  const logout = (event) => {
    event.preventDefault()
    localStorage.removeItem('token')
    //setIsAdmin(false)
    setIsLoggedIn(false)
    //setUserData({email:""})
  }

    return (
      <BrowserRouter>
        <Navigation
          isLoggedIn={isLoggedIn}
          logout={logout}
          //isAdmin={isAdmin}
          //userData={userData}
        />
        <Switch>
          <Route exact path="/" component={Homepage}/>
          <Route exact path="/Portfolio" component={Portfolio} />
          <Route exact path="/Resume" component={Resume} />
          <Route exact path="/Contact" component={Contact} />
          <Route exact path="/Login">
            <Login
              isLoggedIn={isLoggedIn}
              setIsLoggedIn={setIsLoggedIn}
              //setIsAdmin={setIsAdmin}
              //setUserData={setUserData}
            />
          </Route>
          <PrivateRoute path="/Submissions">
            <Listings/>
          </PrivateRoute>
          <PrivateRoute path="/AddUser">
            <AddUser/>
          </PrivateRoute>
          <PrivateRoute path="/EditPortfolio">
            <EditPortfolio/>
          </PrivateRoute>
        </Switch>
        <Footer/> 
      </BrowserRouter>
    );
  }
export default App;
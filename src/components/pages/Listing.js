import React, { useEffect, useState } from 'react'
import { Container, Row, Table } from 'reactstrap'


const Listings = () => {
    const [listing, setListing] = useState([])

    useEffect(()=>{
        async function fetchData(){
            const res = await fetch("http://localhost:3001/contact_form/entries");
            res
            .json()
            .then((res)=>setListing(res))
            .catch((err)=>console.log(err));
        }
        fetchData();

    },[]);
    return (
        <Container>
            <Row>
                <h1>Submissions</h1>
            </Row>
            <Table responsive>
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>PhoneNumber</th>
                    <th>Email</th>
                    <th>Message</th>
                    </tr>
                </thead>
                <tbody>
                    {listing.length === 0 &&
                        <tr><td colSpan="4" className="text-center"><i>No listings found</i></td></tr>
                    }
                    {listing.length > 0 &&
                        listing.map(entry => <tr><td>{entry.entryID}</td><td>{entry.name}</td><td>{entry.phoneNumber}</td><td>{entry.email}</td><td>{entry.content}</td></tr>)
                    }
                </tbody>
            </Table>
        </Container>
    )
}

export default Listings
import React, {useState, useEffect} from 'react'
import { Container, Input, Button, Form, FormGroup, Label, Row, Col } from 'reactstrap'


const EditPortfolio = () => {

    const [projects, setProjects] = useState([]);

    useEffect(()=>{
    async function fetchData(){
        const res = await fetch("http://localhost:3001/portfolio");
        res
        .json()
        .then((res)=>setProjects(res))
        .catch((err)=>console.log(err));
    }
    fetchData();

    },[]);

    const handleChange = (e, row) => {
        const value = e.target.value;
        const name = e.target.name;
        const { projectID } = row;
        const newRows = projects.map(row => {
        if (row.projectID === projectID) {
            return { ...row, [name]: value };
        }
        return row;
        });
        setProjects(newRows);
    };

    const formSubmit = (projectID) => {

        const editedProject = projects.find(project => project.projectID === projectID)

        const response = fetch('http://localhost:3001/portfolio', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(editedProject)
        }).then((response) => alert('Portfolio Change Successful'))
    }

    return (
        <div>
            <Container>
                <h1 className="Page-Header">Edit Portfolio</h1>
                {projects.map((project) => (
                
                <Form key={project.projectID}>
                    <FormGroup>
                        <Row className='project-row'>
                            <Col>
                                <Label>Title</Label>
                                <Input type='text' id='title' className='project-input' name="title" value={project.title} onChange={e => handleChange(e, project)}/>
                                <Label>Description</Label>
                                <Input type='textarea' id='description' className='project-textarea' name="description" value={project.description} onChange={e => handleChange(e, project)}/>
                                <Button color='primary' onClick={() => formSubmit(project.projectID)}>Submit Change</Button>
                            </Col>
                            <Col>
                                <img src={project.picture} className='project-webimage' alt=''/>
                            </Col>
                        </Row>
                    </FormGroup>
                </Form>
                ))}
            </Container>
        </div>
    )
}

export default EditPortfolio
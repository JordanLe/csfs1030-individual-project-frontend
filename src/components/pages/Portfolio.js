import React, {useState, useEffect} from 'react'
import { Container } from 'reactstrap'


const Portfolio = () => {

    const [projects, setProjects] = useState([]);

    useEffect(()=>{
    async function fetchData(){
        const res = await fetch("http://localhost:3001/portfolio");
        res
        .json()
        .then((res)=>setProjects(res))
        .catch((err)=>console.log(err));
    }
    fetchData();

    },[]);

    return (
        <div>
            <Container>
                <h1 className="Page-Header">PORTFOLIO</h1>
                {projects.map((project) => (
                    <div key={project.projectID} className='project'>
                        <img src={project.picture} className='webimage' alt=''/>
                        <div className='overlay'>
                            <div className='overlay-text'>
                                <div className='image-title'>{project.title}</div>
                                <div className='image-description'>{project.description}</div>
                            </div>
                        </div>
                    </div>
                ))}
            </Container>
        </div>
    )
}

export default Portfolio